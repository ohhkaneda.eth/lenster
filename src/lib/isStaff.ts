import { staffs } from 'data/staffs'

export const isStaff = (id: string) => staffs.includes(id)
